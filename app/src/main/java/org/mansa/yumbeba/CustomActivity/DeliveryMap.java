package org.mansa.yumbeba.CustomActivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.mansa.yumbeba.R;

/**
 * Created by mansa on 12/14/15.
 */
public class DeliveryMap extends Activity {

    ImageView mBackBtn;
    TextView mMapTitle;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.delivery_map);

        mBackBtn = (ImageView) findViewById(R.id.back);

        mMapTitle = (TextView) findViewById(R.id.map_title_txt);
        mMapTitle.setText("Delivery Route");

        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
    }


}
