package org.mansa.yumbeba.CustomActivity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import org.angmarch.views.NiceSpinner;
import org.mansa.yumbeba.R;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by mansa on 12/7/15.
 */
public class DriverAccount extends AppCompatActivity {

    TextView mTotalDeliveriesMadeTxt;
    TextView mTotalAmountDeliveredTxt;

    public static int TOTAL_AMOUNT_DELIVERED = 0;
    public static int NUMBER_OF_DELIVERIES = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.driver_account);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NiceSpinner niceSpinner = (NiceSpinner) findViewById(R.id.stats_duration_spinner);
        List<String> dataset = new LinkedList<>(Arrays.asList("Today", "Past Week", "Past Month"));
        niceSpinner.attachDataSource(dataset);

        mTotalAmountDeliveredTxt = (TextView) findViewById(R.id.total_amount_delivered_txt);
        mTotalDeliveriesMadeTxt = (TextView) findViewById(R.id.number_of_deliveries_txt);

        String amountDelivered = "" + TOTAL_AMOUNT_DELIVERED;
        String deliveriesMade = "" + NUMBER_OF_DELIVERIES;

        mTotalAmountDeliveredTxt.setText(amountDelivered);
        mTotalDeliveriesMadeTxt.setText(deliveriesMade);


    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
