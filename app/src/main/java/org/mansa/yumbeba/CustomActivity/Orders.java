package org.mansa.yumbeba.CustomActivity;

/**
 * Created by mansa on 12/4/15.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mansa.yumbeba.AppController;
import org.mansa.yumbeba.util.CardAdapter;
import org.mansa.yumbeba.util.ItemClickSupport;
import org.mansa.yumbeba.util.PopulateOrdersCard;
import org.mansa.yumbeba.R;
import org.mansa.yumbeba.sqlite.DatabaseOperations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Orders extends AppCompatActivity {

    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;
    Context context = this;

    // Log tag
    private static final String TAG = MainActivity.class.getSimpleName();

    public static final String KEY_DRIVER_PHONE = "phone";
    public static final String KEY_AUTH_TOKEN = "Authorization";
    ;

    // Orders json url
    private static final String ORDERS_URL = "http://private-77516-yumdriverappapi.apiary-mock.com/list_orders/";

    private ProgressDialog pDialog;
    private List<PopulateOrdersCard> ordersArrayList = new ArrayList<PopulateOrdersCard>();;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);


        mRecyclerView = (RecyclerView)findViewById(R.id.orders_recycler_view);
        mRecyclerView.setHasFixedSize(true);


        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new CardAdapter(this, ordersArrayList);
        mRecyclerView.setAdapter(mAdapter);


        pDialog = new ProgressDialog(this);
        // Showing progress dialog before making http request
        pDialog.setMessage("Loading...");
        pDialog.show();

        fetchOrders();

    }

    /*@Override
    protected void onResume()
    {
        super.onResume();
        PopulateOrdersCard orderCardItem = new PopulateOrdersCard();

        DatabaseOperations DOP = new DatabaseOperations(context);
        Cursor CR = DOP.getInformation(DOP);
        CR.moveToFirst();

        do {

            orderCardItem.setRestaurant(CR.getString(0));
            orderCardItem.setDestination(CR.getString(7));
            // adding order to orders array cards
            ordersArrayList.add(orderCardItem);

        }while(CR.moveToNext());


    }

    @Override
    protected void onStart()
    {
        super.onStart();
        PopulateOrdersCard orderCardItem = new PopulateOrdersCard();

        DatabaseOperations DOP = new DatabaseOperations(context);
        Cursor CR = DOP.getInformation(DOP);
        CR.moveToFirst();

        do {

            orderCardItem.setRestaurant(CR.getString(0));
            orderCardItem.setDestination(CR.getString(7));
            // adding order to orders array cards
            ordersArrayList.add(orderCardItem);

        }while(CR.moveToNext());


    }*/

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(Orders.this, DriverAccount.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }



    //get json orders and feed it into cards

    public void fetchOrders() {

        final Bundle bundle = getIntent().getExtras();
        final String phoneNumber = (String) bundle.get("phone");
        final String authToken = "Token " + bundle.get("auth_token");

        // Creating volley request obj
        JsonArrayRequest ordersRequest = new JsonArrayRequest(Request.Method.POST,ORDERS_URL,null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        hidePDialog();

// Parsing json
                        for (int i = 0; i < response.length(); i++) {
                            try {



                                JSONObject obj = response.getJSONObject(i);
                                PopulateOrdersCard orderCardItem = new PopulateOrdersCard();


                                String restaurantNameLocation = obj.getJSONObject("restaurant").getString("name") + ",  " + obj.getJSONObject("restaurant").getJSONObject("address").getString("hood");
                                String restaurantStreet = "Street : " + obj.getJSONObject("restaurant").getJSONObject("address").getString("street_name");
                                String restaurantApartment = "Apartment : " + obj.getJSONObject("restaurant").getJSONObject("address").getString("apartment_name") +  ", " + obj.getJSONObject("restaurant").getJSONObject("address").getString("unit_number");
                                String restaurantOtherInfo = "Info : " + obj.getJSONObject("restaurant").getJSONObject("address").getString("other_info");
                                String mRestaurantPhoneNumber =  obj.getJSONObject("restaurant").getString("phone");
                                String mRestaurantLatitude =  obj.getJSONObject("restaurant").getJSONObject("address").getJSONObject("position").getString("longitude");
                                String mRestaurantLongitude =  obj.getJSONObject("restaurant").getJSONObject("address").getJSONObject("position").getString("latitude");

                                String customerLocation = obj.getJSONObject("client").getJSONObject("address").getString("hood") + ", " + obj.getJSONObject("client").getJSONObject("address").getString("street_name");
                                String customerName = "Customer : " + obj.getJSONObject("client").getString("name");
                                String customerApartment = "Apartment : " + obj.getJSONObject("client").getJSONObject("address").getString("apartment_name") +  ", " + obj.getJSONObject("client").getJSONObject("address").getString("unit_number");
                                String customerOtherInfo =    "Info : " + obj.getJSONObject("client").getJSONObject("address").getString("other_info");
                                String mCustomerPhoneNumber = (String) obj.getJSONObject("client").getString("phone");
                                String mCustomerLatitude = (String) obj.getJSONObject("client").getJSONObject("address").getJSONObject("position").getString("latitude");
                                String mCustomerLongitude = (String) obj.getJSONObject("client").getJSONObject("address").getJSONObject("position").getString("longitude");

                                String orderTotal = "Total : KSH " + obj.getString("order_total");
                                String orderInstructions = "\nInstructions : " +obj.getString("order_instructions");
                                String orderId = obj.getString("order_id");

                                int status = obj.getInt("status");
                                String orderParticulars = "";

                                JSONArray orderItems = obj.getJSONArray("order_items");


                                for (int x = 0; x < orderItems.length(); x++) {
                                    try {
                                        JSONObject orderItem = orderItems.getJSONObject(x);
                                        orderParticulars += (orderItem.getString("quantity") + " Pcs of " + orderItem.getString("item") + " @ " + orderItem.getString("price") + "\n");

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                                orderCardItem.setRestaurant(restaurantNameLocation);
                                orderCardItem.setDestination(customerLocation);

// adding order to orders array cards
                                ordersArrayList.add(orderCardItem);


                                DatabaseOperations DB = new DatabaseOperations(context);
                                DB.saveInfo(DB, orderId, restaurantNameLocation, restaurantStreet, restaurantApartment, restaurantOtherInfo,
                                        mRestaurantPhoneNumber, mRestaurantLatitude, mRestaurantLongitude, customerLocation, customerName,
                                        customerApartment, customerOtherInfo, mCustomerPhoneNumber, mCustomerLatitude, mCustomerLongitude, orderTotal,
                                        orderInstructions,orderParticulars);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

// notifying list adapter about data changes
// so that it shows updated data in ListView
                        mAdapter.notifyDataSetChanged();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_DRIVER_PHONE, phoneNumber);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put(KEY_AUTH_TOKEN, authToken);
                //headers.put("Content-Type", "application/json");
                return headers;
            }


        };

// Adding request to request queue
        AppController.getInstance().addToRequestQueue(ordersRequest);


        ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                // do it

                Bundle b = new Bundle();
                b.putInt("position", position);
                Intent i = new Intent(Orders.this, DoggieBag.class);
                i.putExtras(b);
                startActivity(i);
            }
        });


    }


}