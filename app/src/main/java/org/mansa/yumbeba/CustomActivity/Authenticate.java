package org.mansa.yumbeba.CustomActivity;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;
import org.mansa.yumbeba.AppController;
import org.mansa.yumbeba.R;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mansa on 12/4/15.
 */
public class Authenticate extends AppCompatActivity {

    private Button loginBtn;
    private TextInputLayout mPhoneNumberLayout;
    private TextInputLayout mPasswordLayout;
    private EditText mPasswordTxt;
    private EditText mPhoneNumberTxt;
    TextView mYumWelcomeTxt;
    Context context = this;


    private static final String REGISTER_URL = "http://private-77516-yumdriverappapi.apiary-mock.com/authenticate/";

    public static final String KEY_PHONENUMBER = "phone";
    public static final String KEY_PASSWORD = "password";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(AppController.getInstance().isCurrentUser()) {

            Intent intent = new Intent(Authenticate.this, Orders.class);
            intent.putExtra("auth_token", AppController.getInstance().getAuthToken());
            intent.putExtra("phone_number", AppController.getInstance().getDriverNumber());
            startActivity(intent);
            finish();
        }

        setContentView(R.layout.authenticate);


        mPhoneNumberLayout = (TextInputLayout) findViewById(R.id.phone_number_layout);
        mPhoneNumberTxt = (EditText) findViewById(R.id.phone_number_txt);
        mPasswordLayout = (TextInputLayout) findViewById(R.id.password_layout);
        mPasswordTxt = (EditText) findViewById(R.id.password_txt);
        mYumWelcomeTxt = (TextView) findViewById(R.id.yum_welcome_txt);



        loginBtn = (Button) findViewById(R.id.loginBtn);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                authenticateDriver();
            }
        });
    }

    public void authenticateDriver(){

        boolean isPhoneNumberEmpty = phoneNumberEmpty();
        boolean isPasswordEmpty = passwordEmpty();

        if (isPhoneNumberEmpty && isPasswordEmpty){

            Context context = getApplicationContext();
            CharSequence text = "Fill All Slots To Proceed!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
        else if (isPasswordEmpty && !isPhoneNumberEmpty) {

            mPasswordLayout.setError("Fill this to proceed");
            mPhoneNumberLayout.setError(null);
        }
        else if (!isPasswordEmpty && isPhoneNumberEmpty) {

            mPhoneNumberLayout.setError("Fill this to proceed");
            mPasswordLayout.setError(null);
        }
        else {

            if(isDataOn()) {
                registerUser();
            }else {

                showNetworkSettingsAlert();
            }


        }



    }

    private boolean phoneNumberEmpty(){

        return mPhoneNumberTxt.getText() == null || mPhoneNumberTxt.getText().toString() == null || mPhoneNumberTxt.getText().toString().isEmpty();
    }


    private boolean passwordEmpty(){

        return mPasswordTxt.getText() == null || mPasswordTxt.getText().toString() == null || mPasswordTxt.getText().toString().isEmpty();
    }

    private void registerUser(){

        final String phoneNumber = mPhoneNumberTxt.getText().toString().trim();
        final String password = mPasswordTxt.getText().toString().trim();

        JsonObjectRequest authRequest = new JsonObjectRequest(Request.Method.POST, REGISTER_URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            final Intent intent = new Intent(Authenticate.this,
                                    Orders.class);
                            intent.putExtra("auth_token", (String) response.get("token"));
                            intent.putExtra("phone_number", phoneNumber);
                            AppController.getInstance().setCurrentUser();
                            AppController.getInstance().setAuthToken((String) response.get("token"));
                            AppController.getInstance().setDriverNumber(phoneNumber);
                            AppController.getInstance().setCurrentUser();
                            startActivity(intent);
                            finish();
                        }catch (Exception e){}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Authentication error", error.getMessage());
                       // Toast.makeText(Authenticate.this,"Please Switch On Your Data To Proceed",Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put(KEY_PHONENUMBER,phoneNumber);
                params.put(KEY_PASSWORD,password);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(authRequest);

    }


    public boolean isDataOn(){
        ConnectivityManager cm = (ConnectivityManager)
                this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        // if no network is available networkInfo will be null
        // otherwise check if we are connected
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    public void showNetworkSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        alertDialog.setTitle("Data Settings");

        alertDialog.setMessage("Data is not enabled. Do you want to go to settings menu?");

        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                context.startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

}
