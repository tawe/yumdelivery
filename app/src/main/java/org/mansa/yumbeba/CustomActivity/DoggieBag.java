package org.mansa.yumbeba.CustomActivity;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import org.mansa.yumbeba.R;
import org.mansa.yumbeba.sqlite.DatabaseOperations;


/**
 * Created by mansa on 12/8/15.
 */
public class DoggieBag  extends AppCompatActivity {

    TextView mRestaurantHoodTxt;
    TextView mRestaurantStreetTxt;
    TextView mRestaurantApartmentTxt;
    TextView mRestaurantInfoTxt;
    Button mRestaurantCallTxt;
    Button mRestaurantMapTxt;
    TextView mcustomerHoodTxt;
    TextView mCustomerNameTxt;
    TextView mCustomerApartmentTxt;
    TextView mCustomerInfoTxt;
    Button mCustomerCallTxt;
    Button mCustomerMapTxt;
    TextView mOrderTotalTxt;
    Button mDeliveryStatusBtn;
    CardView mOrderTotalCard;
    CardView mRestaurantCard;
    CardView mCustomerCard;
    int statusCount = 0;
    Boolean isGPSEnabled = false;
    protected LocationManager locationManager;

    public static LatLng CUSTOMER_LOCATION;
    public static LatLng RESTAURANT_LOCATION;

    ImageView mOrderMenuImg;
    // Log tag
    private static final String TAG = DoggieBag.class.getSimpleName();

    Context context = this;

    private ProgressDialog pDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doggie_bag);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Bundle b = getIntent().getExtras();
        final int position = b.getInt("position");

        //check whether gps is on
        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        //initialize edit text to get amount received by customer
        final EditText mAmountReceivedTxt = new EditText(this);
        /*LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(convertPixelsToDp(8,DoggieBag.this), 0, convertPixelsToDp(8,DoggieBag.this), 0);
        lp.setMarginStart(convertPixelsToDp(8, DoggieBag.this));
        lp.setMarginEnd(convertPixelsToDp(8, DoggieBag.this));
        mAmountReceivedTxt.setLayoutParams(lp);
*/

        // initialize restaurant txtviews
        mRestaurantHoodTxt = (TextView)findViewById(R.id.restaurant_hood_txt);
        mRestaurantStreetTxt = (TextView)findViewById(R.id.restaurant_street_txt);
        mRestaurantApartmentTxt = (TextView)findViewById(R.id.restaurant_apartment_name_txt);
        mRestaurantInfoTxt = (TextView)findViewById(R.id.restaurant_other_info_txt);
        mRestaurantCallTxt = (Button)findViewById(R.id.restaurant_call_txt);
        mRestaurantMapTxt = (Button)findViewById(R.id.restaurant_map_txt);

        // initialize customer txtviews
        mcustomerHoodTxt = (TextView)findViewById(R.id.customer_hood_txt);
        mCustomerNameTxt = (TextView)findViewById(R.id.customer_name_txt);
        mCustomerApartmentTxt = (TextView)findViewById(R.id.customer_apartment_name_txt);
        mCustomerInfoTxt = (TextView)findViewById(R.id.customer_other_info_txt);
        mCustomerCallTxt = (Button)findViewById(R.id.customer_call_txt);
        mCustomerMapTxt = (Button)findViewById(R.id.customer_map_txt);

        mOrderTotalTxt = (TextView) findViewById(R.id.items_total_price_txt);
        mRestaurantCard = (CardView) findViewById(R.id.restaurant_card);
        mCustomerCard = (CardView) findViewById(R.id.customer_card);
        mOrderTotalCard = (CardView) findViewById(R.id.items_total_price_card);
        mOrderMenuImg = (ImageView) findViewById(R.id.order_items_menu_img);

        mDeliveryStatusBtn = (Button) findViewById(R.id.order_status_btn);


        DatabaseOperations DOP = new DatabaseOperations(context);
        final Cursor CR = DOP.getInformation(DOP);
        CR.moveToPosition(position);

        mRestaurantHoodTxt.setText(CR.getString(0));
        mRestaurantStreetTxt.setText(CR.getString(1));
        mRestaurantApartmentTxt.setText(CR.getString(2));
        mRestaurantInfoTxt.setText(CR.getString(3));
        final String mRestaurantPhoneNumber =  "+" + CR.getString(4);
        Double mRestaurantLatitude = Double.parseDouble(CR.getString(5));
        Double mRestaurantLongitude = Double.parseDouble(CR.getString(6));
        mcustomerHoodTxt.setText(CR.getString(7));
        mCustomerNameTxt.setText(CR.getString(8));
        mCustomerApartmentTxt.setText(CR.getString(9));
        mCustomerInfoTxt.setText(CR.getString(10));
        final String mCustomerPhoneNumber = "+" + CR.getString(11);
        Double mCustomerLatitude = Double.parseDouble(CR.getString(12));
        Double mCustomerLongitude = Double.parseDouble(CR.getString(13));
        mOrderTotalTxt.setText(CR.getString(14));

        final String instructions = CR.getString(15);
        final String orderParticulars = CR.getString(16);


        //getting latlang object for restaurant + customer location
        CUSTOMER_LOCATION = new LatLng(mCustomerLatitude, mCustomerLongitude);
        RESTAURANT_LOCATION = new LatLng(mRestaurantLatitude, mRestaurantLongitude);

        mOrderTotalCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(DoggieBag.this);
                builder.setMessage(orderParticulars + instructions);
                builder.setTitle("ORDER DETAILS");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //to close the dialog
                        dialogInterface.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();


            }
        });

        //restaurant card onclick
        mRestaurantCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                locationDetails("RESTAURANT LOCATION", CR.getString(0), CR.getString(1));


            }
        });

        //customer card onclick
        mCustomerCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                locationDetails("CUSTOMER LOCATION", CR.getString(8), CR.getString(7));


            }
        });

        mOrderMenuImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(DoggieBag.this);
                builder.setMessage(orderParticulars + instructions);
                builder.setTitle("ORDER DETAILS");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //to close the dialog
                        dialogInterface.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();


            }
        });

        mDeliveryStatusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statusCount++;

                switch (statusCount) {
                    case 1:
                        mDeliveryStatusBtn.setText("Order Arrived");
                        break;
                    case 2:
                        mDeliveryStatusBtn.setText("Client Has Paid");
                        // Set the default text to a link of the Queen
                        break;
                    case 3:

                        mAmountReceivedTxt.setHint("KSH");
                        mAmountReceivedTxt.setInputType(InputType.TYPE_CLASS_NUMBER);

                        new AlertDialog.Builder(DoggieBag.this)
                                .setTitle("Money Received")
                                .setMessage("Please Input Money Received From Customer")
                                .setView(mAmountReceivedTxt)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        String amountReceived = mAmountReceivedTxt.getText().toString();
                                        DriverAccount.TOTAL_AMOUNT_DELIVERED += Integer.parseInt(amountReceived);
                                        DriverAccount.NUMBER_OF_DELIVERIES += 1;
                                        dialog.dismiss();
                                        mDeliveryStatusBtn.setVisibility(View.INVISIBLE);
                                    }
                                })
                                /*.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        statusCount = 2;
                                        dialog.dismiss();

                                    }
                                })*/
                                .show();

                        break;
                }
            }
        });


        mCustomerCallTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call(mCustomerPhoneNumber);
            }
        });

        mRestaurantCallTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call(mRestaurantPhoneNumber);
            }
        });



        mCustomerMapTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                drawMap();
            }
        });

        mRestaurantMapTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                drawMap();
            }
        });

    }

    private void call(String phoneNumber) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phoneNumber));

        try{
            startActivity(callIntent);
        }

        catch (android.content.ActivityNotFoundException ex){
            Log.d("Call Operations", ex.getMessage());
           //Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    private void drawMap(){

        //check whether gps is on
        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

          if(isGPSEnabled == true) {

              Intent intent = new Intent(DoggieBag.this, DeliveryMap.class);
              startActivity(intent);
          }else{
              showSettingsAlert();
          }

    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(DoggieBag.this, DriverAccount.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }



    public static int convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int dp = (int) (px / (metrics.densityDpi / 160f));
        return dp;
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        alertDialog.setTitle("GPS Settings");

        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    public void locationDetails(String title, String building, String street){

        AlertDialog.Builder builder = new AlertDialog.Builder(DoggieBag.this);
        builder.setMessage(building + " on " + street);
        builder.setTitle(title);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //to close the dialog
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
