package org.mansa.yumbeba.util;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;


import org.mansa.yumbeba.CustomActivity.DoggieBag;
import org.mansa.yumbeba.R;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;


/**
 * Created by mansa on 12/14/15.
 */
public class RouteMap extends MapFragment  implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnInfoWindowClickListener,
        GoogleMap.OnMapLongClickListener,
        GoogleMap.OnMapClickListener,
        GoogleMap.OnMarkerClickListener{

    private GoogleApiClient mGoogleApiClient;
    private Location mCurrentLocation;
    private final int MAP_TYPE = GoogleMap.MAP_TYPE_HYBRID;


    @Override
    public void onConnected(Bundle bundle) {

        mCurrentLocation = LocationServices
                .FusedLocationApi
                .getLastLocation( mGoogleApiClient );


        initCamera(mCurrentLocation);
        //drawPolygon(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), DoggieBag.RESTAURANT_LOCATION, DoggieBag.CUSTOMER_LOCATION);
        setMarkers();
        initMapRoute();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        //Create a default location if the Google API Client fails. Placing location at yumHeadOffice
        mCurrentLocation = new Location( "" );
        mCurrentLocation.setLatitude( -1.2959978 );
        mCurrentLocation.setLongitude(36.7771151);
        initCamera(mCurrentLocation);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        return true;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        mGoogleApiClient = new GoogleApiClient.Builder( getActivity() )
                .addConnectionCallbacks( this )
                .addOnConnectionFailedListener( this )
                .addApi( LocationServices.API )
                .build();

        initListeners();

    }

    private void initListeners() {
        getMap().setOnMarkerClickListener(this);
        getMap().setOnMapLongClickListener(this);
        getMap().setOnInfoWindowClickListener(this);
        getMap().setOnMapClickListener(this);
    }

    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if( mGoogleApiClient != null && mGoogleApiClient.isConnected() ) {
            mGoogleApiClient.disconnect();
        }
    }


    private void initCamera( Location location ) {


            CameraPosition position = CameraPosition.builder()
                    .target(new LatLng(location.getLatitude(),
                            location.getLongitude()))
                    .zoom(14f)
                    .bearing(0.0f)
                    .tilt(0.0f)
                    .build();

            getMap().animateCamera(CameraUpdateFactory
                    .newCameraPosition(position), null);

            getMap().setMapType(MAP_TYPE);
            getMap().setTrafficEnabled(true);
            getMap().setMyLocationEnabled(true);
            getMap().getUiSettings().setZoomControlsEnabled(true);

    }

    private void setMarkers(){
        MarkerOptions customerLocation = new MarkerOptions().position(DoggieBag.CUSTOMER_LOCATION);
        MarkerOptions restaurantLocation = new MarkerOptions().position(DoggieBag.RESTAURANT_LOCATION);

        customerLocation.title(getAddressFromLatLng(DoggieBag.CUSTOMER_LOCATION));
        restaurantLocation.title(getAddressFromLatLng(DoggieBag.RESTAURANT_LOCATION));

        customerLocation.icon(BitmapDescriptorFactory.defaultMarker());
        restaurantLocation.icon(BitmapDescriptorFactory.defaultMarker());

        /*customerLocation.icon(BitmapDescriptorFactory.fromBitmap(
                BitmapFactory.decodeResource(getResources(),
                        R.drawable.ic_person_pin_circle_black_18dp)));
        restaurantLocation.icon(BitmapDescriptorFactory.fromBitmap(
                BitmapFactory.decodeResource(getResources(),
                        R.drawable.ic_restaurant_menu_black_18dp)));
*/
        getMap().addMarker(customerLocation);
        getMap().addMarker(restaurantLocation);
    }

    private String getAddressFromLatLng( LatLng latLng ) {
        Geocoder geocoder = new Geocoder( getActivity() );

        String address = "";
        try {
            address = geocoder
                    .getFromLocation( latLng.latitude, latLng.longitude, 1 )
                    .get( 0 ).getAddressLine( 0 );
        } catch (IOException e ) {
        }

        return address;
    }

    private void drawPolygon( LatLng driverLocation, LatLng restaurantLocation, LatLng customerLocation ) {

        PolygonOptions options = new PolygonOptions();
        options.add( driverLocation, restaurantLocation, customerLocation );

        options.fillColor( getResources()
                .getColor( R.color.fill_color ));
        options.strokeColor( getResources()
                .getColor( R.color.stroke_color ) );
        options.strokeWidth( 3 );

        getMap().addPolygon( options );
    }


    protected void initMapRoute() {

        //List<LatLng> decodedPath = PolyUtil.decode(polyline);
        List<LatLng> decodedPath = Arrays.asList((new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude())), DoggieBag.RESTAURANT_LOCATION, DoggieBag.CUSTOMER_LOCATION);

        getMap().addPolyline(new PolylineOptions().addAll(decodedPath));
    }


}
