package org.mansa.yumbeba.sqlite;

import android.provider.BaseColumns;

/**
 * Created by mansa on 12/11/15.
 */
public class TableData {

    public TableData(){}

    public static abstract class TableInfo implements BaseColumns
    {

        public static final String USER_NAME = "user_name";
        public static final String USER_PASS = "user_pass";
        public static final String ORDER_ID = "order_id";
        public static final String RESTAURANT_LOCATION = "restaurant_name_location";
        public static final String RESTAURANT_STREET = "restaurant_street";
        public static final String RESTAURANT_APARTMENT = "restaurant_apartment";
        public static final String RESTAURANT_INFO = "restaurant_other_info";
        public static final String RESTAURANT_PHONE = "restaurant_phone_number";
        public static final String RESTAURANT_LONGITUDE = "restaurant_longitude";
        public static final String RESTAURANT_LATITUDE= "restaurant_latitude";
        public static final String CUSTOMER_LOCATION = "customer_location";
        public static final String CUSTOMER_NAME = "customer_name";
        public static final String CUSTOMER_APARTMENT = "customer_apartment";
        public static final String CUSTOMER_INFO = "customer_other_info";
        public static final String CUSTOMER_PHONE = "customer_phone_number";
        public static final String CUSTOMER_LONGITUDE = "customer_longitude";
        public static final String CUSTOMER_LATITUDE = "customer_latitude";
        public static final String ORDER_TOTAL = "order_total";
        public static final String ORDER_INSTRUCTION = "order_instructions";
        public static final String ORDER_PARTICULARS = "order_particulars";

        public static final String DATABASE_NAME = "delivery_info";
        public static final String TABLE_NAME = "orders_info";

    }


}
