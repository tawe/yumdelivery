package org.mansa.yumbeba.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by mansa on 12/11/15.
 */
public class DatabaseOperations extends SQLiteOpenHelper {

    public static final int database_version = 1;
    public String CREATE_QUERY = "CREATE TABLE " + TableData.TableInfo.TABLE_NAME + "(" + TableData.TableInfo.ORDER_ID + " TEXT,"
            + TableData.TableInfo.RESTAURANT_LOCATION + " TEXT,"
            + TableData.TableInfo.RESTAURANT_STREET + " TEXT,"
            + TableData.TableInfo.RESTAURANT_APARTMENT + " TEXT,"
            + TableData.TableInfo.RESTAURANT_INFO + " TEXT,"
            + TableData.TableInfo.RESTAURANT_PHONE + " TEXT,"
            + TableData.TableInfo.RESTAURANT_LATITUDE + " TEXT,"
            + TableData.TableInfo.RESTAURANT_LONGITUDE + " TEXT,"
            + TableData.TableInfo.CUSTOMER_LOCATION + " TEXT,"
            + TableData.TableInfo.CUSTOMER_NAME + " TEXT,"
            + TableData.TableInfo.CUSTOMER_APARTMENT + " TEXT,"
            + TableData.TableInfo.CUSTOMER_INFO + " TEXT,"
            + TableData.TableInfo.CUSTOMER_PHONE + " TEXT,"
            + TableData.TableInfo.CUSTOMER_LATITUDE + " TEXT,"
            + TableData.TableInfo.CUSTOMER_LONGITUDE + " TEXT,"
            + TableData.TableInfo.ORDER_TOTAL + " TEXT,"
            + TableData.TableInfo.ORDER_INSTRUCTION + " TEXT,"
            + TableData.TableInfo.ORDER_PARTICULARS + " TEXT );";

    public DatabaseOperations(Context context) {
        super(context, TableData.TableInfo.DATABASE_NAME, null, database_version);
        Log.d("Database Operation", "Database created");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_QUERY);

        Log.d("Database Operation", "Table created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void saveInfo(DatabaseOperations dop, String orderId, String restaurantNameLocation,
                         String restaurantStreet, String restaurantApartment, String restaurantOtherInfo,
                         String restaurantPhoneNumber,  String restaurantLatitude,String restaurantLongitude,
                         String customerLocation, String customerName, String customerApartment, String customerOtherInfo,
                         String customerPhoneNumber,  String customerLatitude, String customerLongitude, String orderTotal,
                         String orderInstructions, String orderParticulars) {

        SQLiteDatabase SQ = dop.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(TableData.TableInfo.ORDER_ID, orderId);
        cv.put(TableData.TableInfo.RESTAURANT_LOCATION, restaurantNameLocation);
        cv.put(TableData.TableInfo.RESTAURANT_STREET, restaurantStreet);
        cv.put(TableData.TableInfo.RESTAURANT_APARTMENT, restaurantApartment);
        cv.put(TableData.TableInfo.RESTAURANT_INFO, restaurantOtherInfo);
        cv.put(TableData.TableInfo.RESTAURANT_PHONE, restaurantPhoneNumber);
        cv.put(TableData.TableInfo.RESTAURANT_LATITUDE, restaurantLatitude);
        cv.put(TableData.TableInfo.RESTAURANT_LONGITUDE, restaurantLongitude);
        cv.put(TableData.TableInfo.CUSTOMER_LOCATION, customerLocation);
        cv.put(TableData.TableInfo.CUSTOMER_NAME, customerName);
        cv.put(TableData.TableInfo.CUSTOMER_APARTMENT, customerApartment);
        cv.put(TableData.TableInfo.CUSTOMER_INFO, customerOtherInfo);
        cv.put(TableData.TableInfo.CUSTOMER_PHONE, customerPhoneNumber);
        cv.put(TableData.TableInfo.CUSTOMER_LATITUDE, customerLatitude);
        cv.put(TableData.TableInfo.CUSTOMER_LONGITUDE, customerLongitude);
        cv.put(TableData.TableInfo.ORDER_TOTAL, orderTotal);
        cv.put(TableData.TableInfo.ORDER_INSTRUCTION, orderInstructions);
        cv.put(TableData.TableInfo.ORDER_PARTICULARS, orderParticulars);

        long k = SQ.insert(TableData.TableInfo.TABLE_NAME, null, cv);

        Log.d("Database Operation", "One row inserted");



    }

    public Cursor getInformation(DatabaseOperations dop) {

        SQLiteDatabase SQ = dop.getReadableDatabase();
        String[] columns = {TableData.TableInfo.RESTAURANT_LOCATION,
                TableData.TableInfo.RESTAURANT_STREET,
                TableData.TableInfo.RESTAURANT_APARTMENT,
                TableData.TableInfo.RESTAURANT_INFO,
                TableData.TableInfo.RESTAURANT_PHONE,
                TableData.TableInfo.RESTAURANT_LATITUDE,
                TableData.TableInfo.RESTAURANT_LONGITUDE,
                TableData.TableInfo.CUSTOMER_LOCATION,
                TableData.TableInfo.CUSTOMER_NAME,
                TableData.TableInfo.CUSTOMER_APARTMENT,
                TableData.TableInfo.CUSTOMER_INFO,
                TableData.TableInfo.CUSTOMER_PHONE,
                TableData.TableInfo.CUSTOMER_LATITUDE,
                TableData.TableInfo.CUSTOMER_LONGITUDE,
                TableData.TableInfo.ORDER_TOTAL,
                TableData.TableInfo.ORDER_INSTRUCTION,
                TableData.TableInfo.ORDER_PARTICULARS};

        Cursor CR = SQ.query(TableData.TableInfo.TABLE_NAME, columns, null, null, null, null, null);
        return CR;

    }



    public Cursor getUSerPassword(DatabaseOperations DOP, String user) {
        SQLiteDatabase SQ = DOP.getReadableDatabase();
        String selection = TableData.TableInfo.USER_NAME + " LIKE ?";
        String columns[] = {TableData.TableInfo.USER_PASS};
        String args[] = {user};
        Cursor CR = SQ.query(TableData.TableInfo.TABLE_NAME, columns, selection, args, null, null, null, null);
        return CR;
    }

    public void deleteUser(DatabaseOperations DOP, String user, String password){

        String selection = TableData.TableInfo.USER_NAME+ " LIKE ? AND " + TableData.TableInfo.USER_PASS + " LIKE ?";
        //String columns[] = {TableData.TableInfo.USER_PASS};
        String args[] = {user, password};
        SQLiteDatabase SQ = DOP.getWritableDatabase();
        SQ.delete(TableData.TableInfo.TABLE_NAME, selection, args);
    }
}
